<?php

namespace Drupal\og_migrate_group\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

/**
 * The 'd7_og_content' source plugin.
 *
 * @MigrateSource(
 *   id = "d7_og_content",
 *   source_module = "og_migrate_group"
 * )
 */
class D7OGContent extends SqlBase
{

  /**
   * {@inheritdoc}
   */
  public function query()
  {
    $query = $this->select('og_membership', 'og_membership')
      ->fields('og_membership', ['id', 'gid'])
      ->fields('node_og_membership', ['nid', 'type', 'uid', 'title', 'tnid'])
      ->fields('node_og_membership_1', ['type'])
      ->fields('node_og_membership__og_membership', ['language']);
    $query->innerJoin('node', 'node_og_membership', 'og_membership.etid=node_og_membership.nid AND og_membership.entity_type=\'node\'');
    $query->leftJoin('node', 'node_og_membership_1', 'og_membership.gid=node_og_membership_1.nid');
    $query->leftJoin('og_membership', 'node_og_membership__og_membership', 'node_og_membership.nid=node_og_membership__og_membership.etid AND node_og_membership__og_membership.entity_type=\'node\'');
    $query->condition('node_og_membership.type', ['blog', 'podcast', 'proposta'], 'IN');
    $query->condition('node_og_membership_1.type', ['bloc', 'feed', 'feed_proposta'], 'IN');

    //$results = $query->execute()->fetchAll();
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields()
  {
    return [
      'og_membership_id' => $this->t('The og membership id.'),
      'content_nid' => $this->t('The node ID of the content.'),
      'content_title' => $this->t('The content node title.'),
      'content_bundle' => $this->t('The content node type (bundle)'),
      'content_uid' => $this->t('The content node author\'s uid'),
      'group_type' => $this->t('The group type'),
      'language' => $this->t('The content language'),

    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds()
  {
    $ids = [
      'id' => [
        'type' => 'integer',
        'alias' => 'og_membership'
      ]
    ];
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row)
  {

    // @DCG
    // Extend/modify the row here if needed.
    //
    // Example:
    // @code
    // $name = $row->getSourceProperty('name');
    // $row->setSourceProperty('name', Html::escape('$name');
    // @endcode
    $row->setSourceProperty('og_membership_id', $row->getSourceProperty('id'));
    $row->setSourceProperty('content_nid', $row->getSourceProperty('nid'));
    $row->setSourceProperty('content_bundle', $row->getSourceProperty('type'));
    $row->setSourceProperty('content_uid', $row->getSourceProperty('uid'));
    $row->setSourceProperty('content_title', $row->getSourceProperty('title'));
    $row->setSourceProperty('group_type', $row->getSourceProperty('node_og_membership_1_type'));
    $row->setSourceProperty('language', $row->getSourceProperty('content_language'));
    return parent::prepareRow($row);
  }
}
