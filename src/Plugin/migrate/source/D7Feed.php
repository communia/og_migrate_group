<?php

namespace Drupal\og_migrate_group\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Component\Utility\Html;

/**
 * The 'd7_feed' source plugin.
 *
 * @MigrateSource(
 *   id = "d7_feed",
 *   source_module = "og_migrate_group"
 * )
 */
class D7Feed extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('node', 'node')
      ->fields('node', ['nid', 'title', 'type'])
      ->fields('feeds_source', ['config']);
    $query->leftJoin('feeds_source', 'feeds_source', 'feeds_source.feed_nid=node.nid');
    $query->condition('node.type', ['feed','feed_proposta'], 'IN');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'nid' => $this->t('The node ID.'),
      'title' => $this->t('The node title.'),
      'type' => $this->t('The node type (bundle)'),
      'config' => $this->t('The feed url'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids = [
      'nid' => [
        'type' => 'integer'
      ]
    ];
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {

    // @DCG
    // Extend/modify the row here if needed.
    //
    // Example:
    // @code
    // $name = $row->getSourceProperty('name');
    // $row->setSourceProperty('name', Html::escape('$name');
    // @endcode
    $feed_serial_config = $row->getSourceProperty('config');
    $feed_config = unserialize($feed_serial_config);
    $feed_source = (isset($feed_config['FeedsHTTPFetcher']['source']))? $feed_config['FeedsHTTPFetcher']['source'] : "";
    $source = $row->setSourceProperty('source', Html::escape(UrlHelper::stripDangerousProtocols($feed_source)));
    return parent::prepareRow($row);
  }

}
