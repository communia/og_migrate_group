<?php

namespace Drupal\og_migrate_group\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\MigrateSkipProcessException;

use Drupal\migrate\Row;

/**
 * Skip if user_key as uid is the creator of group with gid as in gid_key.
 *
 * @MigrateProcessPlugin(
 *   id = "skip_creator"
 * )
 *
 * To skip if creator use the following:
 *
 * @code
 * field_creator:
 *   plugin: skip_creator
 *   user_key: uid
 *   gid_key: gid
 *   method: row
 * @endcode
 *
 */
class SkipCreator extends ProcessPluginBase {

  /**
   * Stops processing the current property when value is not set.
   *
   * @param mixed $value
   *   The input value.
   * @param \Drupal\migrate\MigrateExecutableInterface $migrate_executable
   *   The migration in which this process is being executed.
   * @param \Drupal\migrate\Row $row
   *   The row from the source to process.
   * @param string $destination_property
   *   The destination property currently worked on. This is only used together
   *   with the $row above.
   *
   * @return mixed
   *   The input value, $value, if it is not empty.
   *
   * @throws \Drupal\migrate\MigrateSkipProcessException
   *   Thrown if the source property is not set and rest of the process should
   *   be skipped.
   */
  public function process($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $group = \Drupal::entityTypeManager()
      ->getStorage('group')
      ->load($row->getSource()[$this->configuration["gid_key"]]);
    if (!$group) {
      throw new MigrateException('gid not found.');
    }
    #\Drush\Drush::output()->writeln("user id of gid" . $value . " is : " . $group->getOwner()->id());
    if ($group->getOwner()->id() == $row->getSource()[$this->configuration["user_key"]]) {
      throw new MigrateSkipRowException("Creator is the same uid, so it exists");
    }
    return $value;
  }

  /**
   * Skips the current row when value is not set.
   *
   * @param mixed $value
   *   The input value.
   * @param \Drupal\migrate\MigrateExecutableInterface $migrate_executable
   *   The migration in which this process is being executed.
   * @param \Drupal\migrate\Row $row
   *   The row from the source to process.
   * @param string $destination_property
   *   The destination property currently worked on. This is only used together
   *   with the $row above.
   *
   * @return mixed
   *   The input value, $value, if it is not empty.
   *
   * @throws \Drupal\migrate\MigrateSkipRowException
   *   Thrown if the source property is not set and the row should be skipped,
   *   records with STATUS_IGNORED status in the map.
   */
  public function row($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $group = \Drupal::entityTypeManager()
      ->getStorage('group')
      ->load($row->getSource()[$this->configuration["gid_key"]]);
    if (!$group) {
      throw new MigrateException('gid not found.');
    }
    #\Drush\Drush::output()->writeln("user id of gid" . $value . " is : " . $group->getOwner()->id());
    if ($group->getOwner()->id() == $row->getSource()[$this->configuration["user_key"]]) {
      throw new MigrateSkipRowException("Creator is the same uid, so it exists");
    }
    return $value;
  }

}
