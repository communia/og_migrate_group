<?php

namespace Drupal\og_migrate_group\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateException;
use Drupal\migrate\Row;

/**
 * Gets user creator id from group id.
 *
 * @MigrateProcessPlugin(
 *   id = "group_creator"
 * )
 *
 * To get group creator use the following:
 *
 * @code
 * field_creator:
 *   plugin: group_creator
 *   source: gid
 * @endcode
 *
 */
class GroupCreator extends ProcessPluginBase {
  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Throw an error if value and reverse value are the same.
    $group = \Drupal::entityTypeManager()
      ->getStorage('group')
      ->load($value);
    if (!$group) {
      throw new MigrateException('gid not found.');
    }
    #\Drush\Drush::output()->writeln("user id of gid" . $value . " is : " . $group->getOwner()->id());
    return $group->getOwner()->id();
  }
}
